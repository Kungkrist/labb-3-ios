//
//  TodoListTableViewController.m
//  Todo List
//
//  Created by DEA on 2016-02-10.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "TodoListTableViewController.h"
#import "Event.h"
#import "AddViewController.h"
#import "EventList.h"
#import "EventDetailsViewController.h"

@interface TodoListTableViewController ()
@property (nonatomic) EventList *events;
@end

@implementation TodoListTableViewController



-(void) viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void) save{
    NSData *encodedList = [NSKeyedArchiver archivedDataWithRootObject:self.events];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedList forKey:@"eventList"];
    [defaults synchronize];
    
}

- (void) load {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedList = [defaults objectForKey:@"eventList"];
    EventList *list = [NSKeyedUnarchiver unarchiveObjectWithData:encodedList];
    
    if(list.normalEvents.count > 0 || list.importantEvents.count > 0) {
        self.events = list;
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.events = [[EventList alloc]init];
    
    [self.events.importantEvents addObject: [[Event alloc] initWithName:@"ImportantExample" andDescription:@"This is an example for an important view" andIsImportant:YES]];
    
    [self.events.normalEvents addObject: [[Event alloc] initWithName:@"NormalExample" andDescription:@"This is an example for an normal view" andIsImportant:NO]];
    
    [self load];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return self.events.importantEvents.count;
    }
    
    if(section == 1)
        return self.events.normalEvents.count;
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;
    
    if(indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ImportantCell"];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", self.events.importantEvents[indexPath.row]];
        
        return cell;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NormalCell"];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", self.events.normalEvents[indexPath.row]];
        return cell;
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if(indexPath.section == 0) {
            [self.events.importantEvents removeObjectAtIndex:indexPath.row];
        } else {
            [self.events.normalEvents removeObjectAtIndex:indexPath.row];
        }
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];

    if([segue.identifier isEqualToString:@"ImportantDetailsIdentifier"]) {
        EventDetailsViewController *destination = [segue destinationViewController];
        UITableViewCell *cell = sender;
        destination.title = cell.textLabel.text;
        destination.event = self.events.importantEvents[indexPath.row];
        
    } else if([segue.identifier isEqualToString:@"NormalDetailsIdentifier"]) {
        EventDetailsViewController *destination = [segue destinationViewController];
        UITableViewCell *cell = sender;
        destination.title = cell.textLabel.text;
        destination.event = self.events.normalEvents[indexPath.row];
        
    
    } else if([segue.identifier isEqualToString:@"AddIdentifier"]) {
        AddViewController *view = [segue destinationViewController];
        view.events = self.events;
        
    }
}


@end
