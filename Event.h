//
//  Event.h
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject
@property () NSString *eventName;
@property () NSString *eventDescription;
@property () BOOL isImportant;

-(instancetype) initWithName:(NSString *) eventName
                andDescription: (NSString *) eventDescription
                andIsImportant: (BOOL) isImportant;

- (void)encodeWithCoder:(NSCoder *)encoder;

- (id)initWithCoder:(NSCoder *)decoder;
@end
