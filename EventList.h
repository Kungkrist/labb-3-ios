//
//  EventList.h
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventList : NSObject
@property (nonatomic) NSMutableArray *importantEvents;
@property (nonatomic) NSMutableArray *normalEvents;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;
@end
