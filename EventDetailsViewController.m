//
//  EventDetailsViewController.m
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "EventDetailsViewController.h"

@interface EventDetailsViewController ()
@property (weak, nonatomic) IBOutlet UITextView *eventDescription;

@end

@implementation EventDetailsViewController

-(Event *) events {
    if(_event)
        _event = [[Event alloc]init];
    
    return _event;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.eventDescription.text = self.event.eventDescription;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
