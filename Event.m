//
//  Event.m
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "Event.h"

@implementation Event

-(instancetype) initWithName:(NSString *)eventName
                andDescription:(NSString *)eventDescription
                andIsImportant:(BOOL)isImportant
{
    self = [super init];
    
    if(self) {
        self.eventName = eventName;
        self.eventDescription = eventDescription;
        self.isImportant = isImportant;
    }
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.eventName forKey:@"eventName"];
    [encoder encodeObject:self.eventDescription forKey:@"eventDescription"];
    [encoder encodeObject:[NSString stringWithFormat:@"%s", self.isImportant ? "YES" : "NO"] forKey:@"isImportant"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.eventName = [decoder decodeObjectForKey:@"eventName"];
        self.eventDescription = [decoder decodeObjectForKey:@"eventDescription"];
        NSString *boolValue = [decoder decodeObjectForKey:@"isImportant"];
        if([boolValue containsString:@"YES"]) {
            self.isImportant = YES;
        } else {
            self.isImportant = NO;
        }
    }
    return self;
}

-(NSString *) description {
    
    if(self.isImportant)
        return [NSString stringWithFormat:@"%@ (important)", self.eventName];
    
    return [NSString stringWithFormat:@"%@", self.eventName];
}

@end
