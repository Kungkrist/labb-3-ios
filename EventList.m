//
//  EventList.m
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "EventList.h"

@implementation EventList

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.importantEvents forKey:@"importantEvents"];
    [encoder encodeObject:self.normalEvents forKey:@"normalEvents"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.importantEvents = [decoder decodeObjectForKey:@"importantEvents"];
        self.normalEvents = [decoder decodeObjectForKey:@"normalEvents"];
    }
    return self;
}

-(NSMutableArray *) importantEvents {
    if(!_importantEvents)
        _importantEvents = [[NSMutableArray alloc] init];
    return _importantEvents;
}

-(NSMutableArray *) normalEvents {
    if(!_normalEvents)
        _normalEvents = [[NSMutableArray alloc] init];
    return _normalEvents;
}
@end
