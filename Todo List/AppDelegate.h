//
//  AppDelegate.h
//  Todo List
//
//  Created by DEA on 2016-02-10.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

