//
//  AddViewController.m
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "AddViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface AddViewController ()
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITextField *eventNameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *isImportantSwitch;

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpDescription];
    self.isImportantSwitch.on = NO;
}

-(void)setUpDescription {
    self.descriptionTextView.layer.borderWidth = 1.0f;
    self.descriptionTextView.layer.borderColor = [[UIColor blackColor] CGColor];
}

- (IBAction)addClick:(UIButton *)sender {
    if(self.isImportantSwitch.on) {
        [self.events.importantEvents addObject:[[Event alloc] initWithName:self.eventNameTextField.text
                                                            andDescription:self.descriptionTextView.text
                                                            andIsImportant:self.isImportantSwitch.on]];
    } else {
        [self.events.normalEvents addObject:[[Event alloc] initWithName:self.eventNameTextField.text
                                                            andDescription:self.descriptionTextView.text
                                                            andIsImportant:self.isImportantSwitch.on]];
         
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
