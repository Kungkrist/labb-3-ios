//
//  main.m
//  Todo List
//
//  Created by DEA on 2016-02-10.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
