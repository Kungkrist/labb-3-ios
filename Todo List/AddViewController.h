//
//  AddViewController.h
//  Todo List
//
//  Created by DEA on 2016-02-14.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Event.h"
#import "EventList.h"

@interface AddViewController : UIViewController

@property () EventList *events;
@end
